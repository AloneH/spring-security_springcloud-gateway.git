/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : rbac

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 27/07/2020 20:17:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '修改者ID',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '乐观锁',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (1, '/user/save', '增加用户', NULL, '2020-07-26 01:38:26', NULL, '2020-07-26 01:38:26', 1, 0);
INSERT INTO `permission` VALUES (2, '/user/update', '修改用户', NULL, '2020-07-26 01:38:26', NULL, '2020-07-26 01:38:26', 1, 0);
INSERT INTO `permission` VALUES (3, '/user/remove', '删除用户', NULL, '2020-07-26 01:39:26', NULL, '2020-07-26 01:39:26', 1, 0);
INSERT INTO `permission` VALUES (4, '/user/list', '查询用户', NULL, '2020-07-26 01:39:55', NULL, '2020-07-26 01:39:55', 1, 0);
INSERT INTO `permission` VALUES (5, '/role/add', '增加角色', NULL, '2020-07-26 01:40:14', NULL, '2020-07-26 01:40:14', 1, 0);
INSERT INTO `permission` VALUES (6, '/role/update', '修改角色', NULL, '2020-07-26 01:40:26', NULL, '2020-07-26 01:40:26', 1, 0);
INSERT INTO `permission` VALUES (7, '/role/remove', '删除角色', NULL, '2020-07-26 01:40:41', NULL, '2020-07-26 01:40:41', 1, 0);
INSERT INTO `permission` VALUES (8, '/role/list', '查询角色', NULL, '2020-07-26 01:40:56', NULL, '2020-07-26 01:40:56', 1, 0);
INSERT INTO `permission` VALUES (9, '/permission/save', '增加权限', NULL, '2020-07-26 01:41:28', NULL, '2020-07-26 01:41:28', 1, 0);
INSERT INTO `permission` VALUES (10, '/permission/update', '修改权限', NULL, '2020-07-26 01:41:40', NULL, '2020-07-26 01:41:40', 1, 0);
INSERT INTO `permission` VALUES (11, '/permission/remov', '删除权限', NULL, '2020-07-26 01:42:00', NULL, '2020-07-26 01:42:23', 2, 0);
INSERT INTO `permission` VALUES (12, '/permission/list', '查询权限', NULL, '2020-07-26 01:42:10', NULL, '2020-07-26 01:42:10', 1, 0);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '修改者ID',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '乐观锁',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'user1', '普通用户', NULL, '2020-07-26 00:04:35', NULL, '2020-07-26 23:40:19', 5, 1);
INSERT INTO `role` VALUES (2, 'user_manager', '用户管理员', NULL, '2020-07-26 00:04:36', NULL, '2020-07-27 00:57:32', 1, 0);
INSERT INTO `role` VALUES (3, 'permission_manager', '权限管理员', NULL, '2020-07-26 00:05:37', NULL, '2020-07-26 00:05:37', 1, 0);
INSERT INTO `role` VALUES (4, 'role_manager', '角色管理员', NULL, '2020-07-26 00:05:37', NULL, '2020-07-26 00:05:37', 1, 0);
INSERT INTO `role` VALUES (5, 'admin', '系统管理员', NULL, '2020-07-26 00:22:37', NULL, '2020-07-26 21:41:52', 2, 0);
INSERT INTO `role` VALUES (6, 'admin', '系统管理员2', NULL, '2020-07-26 00:23:04', NULL, '2020-07-26 00:23:04', 1, 1);
INSERT INTO `role` VALUES (7, '123', '123', NULL, '2020-07-26 01:05:51', NULL, '2020-07-26 01:05:51', 1, 1);
INSERT INTO `role` VALUES (8, '111', '111', NULL, '2020-07-26 01:06:24', NULL, '2020-07-26 01:06:24', 1, 1);
INSERT INTO `role` VALUES (9, '333', '333', NULL, '2020-07-26 01:06:48', NULL, '2020-07-26 01:06:48', 1, 1);

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `rid` int(11) NOT NULL COMMENT '角色ID',
  `pid` int(11) NOT NULL COMMENT '权限ID',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '修改者ID',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '乐观锁',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_role_permission_rid`(`rid`) USING BTREE,
  INDEX `fk_role_permission_pid`(`pid`) USING BTREE,
  CONSTRAINT `fk_role_permission_pid` FOREIGN KEY (`pid`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role_permission_rid` FOREIGN KEY (`rid`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES (1, 5, 1, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (2, 5, 2, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (3, 5, 3, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (4, 5, 4, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (5, 5, 5, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (6, 4, 1, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (7, 4, 2, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (8, 4, 3, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (9, 4, 4, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (10, 4, 5, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (11, 3, 1, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (12, 3, 2, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (13, 3, 3, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (14, 3, 4, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (15, 3, 5, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `role_permission` VALUES (16, 1, 4, NULL, '2020-07-26 23:37:34', NULL, '2020-07-26 23:37:34', 1, 1);
INSERT INTO `role_permission` VALUES (17, 1, 1, NULL, '2020-07-26 23:40:19', NULL, '2020-07-26 23:40:19', 1, 0);
INSERT INTO `role_permission` VALUES (18, 2, 1, NULL, '2020-07-27 00:57:32', NULL, '2020-07-27 00:57:32', 1, 0);
INSERT INTO `role_permission` VALUES (19, 2, 2, NULL, '2020-07-27 00:57:32', NULL, '2020-07-27 00:57:32', 1, 0);
INSERT INTO `role_permission` VALUES (20, 2, 3, NULL, '2020-07-27 00:57:32', NULL, '2020-07-27 00:57:32', 1, 0);
INSERT INTO `role_permission` VALUES (21, 2, 4, NULL, '2020-07-27 00:57:32', NULL, '2020-07-27 00:57:32', 1, 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nickname` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '乐观锁',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'token',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'aaaaaa', 'aaaaaa', 'aaa啊', NULL, '2020-07-27 00:18:25', 11, 0, NULL);
INSERT INTO `user` VALUES (2, 'bbb', 'bbb', 'bbb', '2020-07-25 04:27:08', '2020-07-25 04:27:08', 1, 0, NULL);
INSERT INTO `user` VALUES (3, 'ccc', 'ccc', 'ccc', '2020-07-25 04:38:08', '2020-07-25 04:38:08', 1, 0, NULL);
INSERT INTO `user` VALUES (4, 'ddd', 'ddd', 'dd', '2020-07-25 04:38:53', '2020-07-25 04:42:10', 1, 0, NULL);
INSERT INTO `user` VALUES (5, 'eee', 'eee', 'eee', '2020-07-25 06:42:43', '2020-07-25 06:42:43', 1, 0, NULL);
INSERT INTO `user` VALUES (6, 'fff', 'fff', 'fff', '2020-07-25 06:43:14', '2020-07-25 06:43:14', 1, 0, NULL);
INSERT INTO `user` VALUES (7, 'ggg', 'gg', 'ggg', '2020-07-25 06:43:14', '2020-07-25 06:43:14', 1, 0, NULL);
INSERT INTO `user` VALUES (8, 'hh', 'hh', 'hh', '2020-07-25 06:44:42', '2020-07-25 07:38:56', 5, 0, NULL);
INSERT INTO `user` VALUES (9, 'iii', 'iii', 'iii', '2020-07-25 06:44:42', '2020-07-25 06:44:42', 1, 0, NULL);
INSERT INTO `user` VALUES (10, 'jjj', 'jjj', 'jjj', '2020-07-25 21:16:09', '2020-07-25 21:16:09', 1, 1, NULL);
INSERT INTO `user` VALUES (11, 'jjj', 'jjj', 'jjj', '2020-07-25 21:16:09', '2020-07-25 21:16:09', 1, 0, NULL);
INSERT INTO `user` VALUES (12, 'kkk', 'kkk', 'kkk', '2020-07-25 21:16:14', '2020-07-25 21:16:14', 1, 0, NULL);
INSERT INTO `user` VALUES (13, 'kkk', 'kkk', 'kkk', '2020-07-25 21:16:14', '2020-07-25 21:16:14', 1, 0, NULL);
INSERT INTO `user` VALUES (14, 'lll', 'lll', 'lll', '2020-07-25 21:16:19', '2020-07-25 21:16:19', 1, 0, NULL);
INSERT INTO `user` VALUES (15, 'lll', 'lll', 'lll', '2020-07-25 21:16:19', '2020-07-25 21:16:19', 1, 0, NULL);
INSERT INTO `user` VALUES (16, 'mmm', 'mmm', 'mmm', '2020-07-25 21:16:25', '2020-07-25 21:16:25', 1, 0, NULL);
INSERT INTO `user` VALUES (17, 'mmm', 'mmm', 'mmm', '2020-07-25 21:16:25', '2020-07-25 21:16:25', 1, 0, NULL);
INSERT INTO `user` VALUES (18, 'nnn', 'nnn', 'nnn', '2020-07-25 21:16:30', '2020-07-25 21:16:30', 1, 0, NULL);
INSERT INTO `user` VALUES (19, 'nnn', 'nnn', 'nnn', '2020-07-25 21:16:30', '2020-07-25 21:16:30', 1, 0, NULL);
INSERT INTO `user` VALUES (20, 'ooo', 'ooo', 'ooo', '2020-07-25 21:16:35', '2020-07-25 21:16:35', 1, 0, NULL);
INSERT INTO `user` VALUES (21, 'ooo', 'ooo', 'ooo', '2020-07-25 21:16:35', '2020-07-25 21:16:35', 1, 0, NULL);
INSERT INTO `user` VALUES (22, 'ppp', 'ppp', 'ppp', '2020-07-25 21:16:40', '2020-07-25 21:16:40', 1, 0, NULL);
INSERT INTO `user` VALUES (23, 'ppp', 'ppp', 'ppp', '2020-07-25 21:16:40', '2020-07-25 21:16:40', 1, 0, NULL);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `rid` int(11) NOT NULL COMMENT '角色ID',
  `create_by` int(11) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) NULL DEFAULT NULL COMMENT '修改者ID',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NOT NULL DEFAULT 1 COMMENT '乐观锁',
  `deleted` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_user_role_uid`(`uid`) USING BTREE,
  INDEX `fk_user_role_rid`(`rid`) USING BTREE,
  CONSTRAINT `fk_user_role_rid` FOREIGN KEY (`rid`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_role_uid` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 1, 1, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `user_role` VALUES (2, 1, 2, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `user_role` VALUES (3, 1, 3, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `user_role` VALUES (4, 1, 4, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `user_role` VALUES (5, 2, 1, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (6, 2, 2, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (7, 2, 3, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (8, 2, 3, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (9, 1, 5, NULL, NULL, NULL, NULL, 1, 1);
INSERT INTO `user_role` VALUES (10, 4, 3, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (11, 4, 1, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (12, 5, 1, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (13, 5, 4, NULL, NULL, NULL, NULL, 1, 0);
INSERT INTO `user_role` VALUES (14, 1, 2, NULL, '2020-07-27 00:16:02', NULL, '2020-07-27 00:16:02', 1, 1);
INSERT INTO `user_role` VALUES (15, 1, 4, NULL, '2020-07-27 00:16:02', NULL, '2020-07-27 00:16:02', 1, 1);
INSERT INTO `user_role` VALUES (16, 1, 5, NULL, '2020-07-27 00:16:02', NULL, '2020-07-27 00:16:02', 1, 1);
INSERT INTO `user_role` VALUES (17, 1, 5, NULL, '2020-07-27 00:16:17', NULL, '2020-07-27 00:16:17', 1, 1);
INSERT INTO `user_role` VALUES (18, 1, 3, NULL, '2020-07-27 00:16:17', NULL, '2020-07-27 00:16:17', 1, 1);
INSERT INTO `user_role` VALUES (19, 1, 3, NULL, '2020-07-27 00:18:00', NULL, '2020-07-27 00:18:00', 1, 1);
INSERT INTO `user_role` VALUES (20, 1, 4, NULL, '2020-07-27 00:18:00', NULL, '2020-07-27 00:18:00', 1, 1);
INSERT INTO `user_role` VALUES (21, 1, 5, NULL, '2020-07-27 00:18:00', NULL, '2020-07-27 00:18:00', 1, 1);
INSERT INTO `user_role` VALUES (22, 1, 3, NULL, '2020-07-27 00:18:19', NULL, '2020-07-27 00:18:19', 1, 1);
INSERT INTO `user_role` VALUES (23, 1, 4, NULL, '2020-07-27 00:18:19', NULL, '2020-07-27 00:18:19', 1, 1);
INSERT INTO `user_role` VALUES (24, 1, 3, NULL, '2020-07-27 00:18:25', NULL, '2020-07-27 00:18:25', 1, 0);
INSERT INTO `user_role` VALUES (25, 1, 4, NULL, '2020-07-27 00:18:25', NULL, '2020-07-27 00:18:25', 1, 0);
INSERT INTO `user_role` VALUES (26, 1, 2, NULL, '2020-07-27 00:18:25', NULL, '2020-07-27 00:18:25', 1, 0);
INSERT INTO `user_role` VALUES (27, 1, 5, NULL, '2020-07-27 00:18:25', NULL, '2020-07-27 00:18:25', 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
