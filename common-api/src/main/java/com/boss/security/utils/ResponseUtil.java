package com.boss.security.utils;

import com.alibaba.fastjson.JSONObject;
import com.boss.security.model.ResultType;
import com.boss.security.response.Response;

/**
 * 统一响应结果 工具类
 *
 * @author AloneH
 * @date 2020-07-25
 */
public class ResponseUtil {

    /**
     * 获得响应字符串
     * @param content
     * @param code
     * @return
     */
    public static String getResponse(Object content, ResultType code) {
        Response outputParam = new Response();
        outputParam.setData(content);
        outputParam.setStatus(code.getCode());
        outputParam.setMsg(code.getMsg());
        return JSONObject.toJSONString(outputParam);
    }

    /**
     * 获得响应字符串
     * @param code
     * @return
     */
    public static String getResponse(ResultType code) {
        Response outputParam = new Response();
        outputParam.setData(new Object());
        outputParam.setStatus(code.getCode());
        outputParam.setMsg(code.getMsg());
        return JSONObject.toJSONString(outputParam);
    }

    /**
     * 获得响应字符串
     * @param code
     * @param msg
     * @param content
     * @return
     */
    public static String getResponse(Integer code, String msg, Object content) {
        Response outputParam = new Response();
        outputParam.setData(content);
        outputParam.setStatus(code);
        outputParam.setMsg(msg);
        return JSONObject.toJSONString(outputParam);
    }

    /**
     * 获得响应字符串
     * @param code
     * @param msg
     * @return
     */
    public static String getResponse(Integer code, String msg) {
        Response outputParam = new Response();
        outputParam.setData(new Object());
        outputParam.setStatus(code);
        outputParam.setMsg(msg);
        return JSONObject.toJSONString(outputParam);
    }

}
