package com.boss.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.security.po.PermissionPO;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */

public interface PermissionDao extends BaseMapper<PermissionPO> {
    /**
     * 通过Rid读取权限列表
     * @param rid
     * @return
     */
    List<PermissionPO> getByRid(Serializable rid);

    /**
     * 通过Rid删除权限列表
     * @param rid
     * @return
     */
    Integer deleteByRid(Serializable rid);
}
