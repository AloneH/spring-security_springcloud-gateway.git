package com.boss.security.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boss.security.po.RolePO;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
public interface RoleDao extends BaseMapper<RolePO> {

    /**
     * 通过UID读取
     * @param uid
     * @return
     */
    List<RolePO> selectByUid(Serializable uid);

    /**
     * 分页读取
     * @param page
     * @param queryWrapper
     * @param <E>
     * @return
     */
    @Override
    <E extends IPage<RolePO>> E selectPage(@Param("page") E page,@Param("ew")  Wrapper<RolePO> queryWrapper);

    /**
     * 根据Id更新
     * @param entity
     * @return
     */
    @Override
    int updateById(RolePO entity);
}
