package com.boss.security.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.security.po.UserRolePO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author AloneH
 * @since 2020-07-27
 */
public interface UserRoleDao extends BaseMapper<UserRolePO> {

}
