package com.boss.security.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boss.security.po.UserPO;
import org.apache.ibatis.annotations.*;

import java.io.Serializable;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
public interface UserDao extends BaseMapper<UserPO> {

    UserPO selectByUsername(Serializable username);

    @Override
    int updateById(UserPO entity);

    @Override
    <E extends IPage<UserPO>> E selectPage(@Param("page") E page,@Param("ew") Wrapper<UserPO> queryWrapper);

}
