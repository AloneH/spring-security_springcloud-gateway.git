package com.boss.security.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName PageVo
 * @Author AloneH
 * @Date 2020/7/26 10:00
 * @Description VO实体
 **/

@Data
@EqualsAndHashCode(callSuper = false)
public class PageVO implements Serializable {

    /**
     * 查询数据列表
     */
    protected List<Object> items;

    /**
     * 总数
     */
    protected long total;

    /**
     * 每页显示条数，默认 10
     */
    protected long limit;

    /**
     * 当前页
     */
    protected long page;


}
