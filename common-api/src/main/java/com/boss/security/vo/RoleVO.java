package com.boss.security.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.boss.security.po.PermissionPO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleVO implements Serializable {

    /**
     * ID
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 角色
     */
    private String role;

    /**
     * 权限列表
     */
    private List<PermissionVO> permissions;

}
