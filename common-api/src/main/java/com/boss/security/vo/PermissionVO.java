package com.boss.security.vo;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PermissionVO implements Serializable {

    /**
     * ID
     */
    private Integer id;

    /**
     * 地址
     */
    private String url;

    /**
     * 名称
     */
    private String name;

}
