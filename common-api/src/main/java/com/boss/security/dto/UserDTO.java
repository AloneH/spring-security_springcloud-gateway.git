package com.boss.security.dto;

import java.io.Serializable;
import java.util.List;

import com.boss.security.po.RolePO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDTO implements Serializable {

    /**
     * ID
     */
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 角色列表
     */
    private List<RolePO> roles;

    /**
     * Token
     */
    private String token;


}
