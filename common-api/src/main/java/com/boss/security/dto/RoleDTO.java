package com.boss.security.dto;

import java.io.Serializable;
import java.util.List;

import com.boss.security.po.PermissionPO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RoleDTO implements Serializable {

    /**
     * ID
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 角色
     */
    private String role;

    /**
     * 权限列表
     */
    private List<PermissionPO> permissions;

}
