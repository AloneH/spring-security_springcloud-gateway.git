package com.boss.security.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @ClassName PageDTO
 * @Author AloneH
 * @Date 2020/7/26 10:04
 * @Description
 * 实体类 DTO
 **/

@Data
@EqualsAndHashCode(callSuper = false)
public class PageDTO {

    /**
     * 查询数据列表
     */
    protected List<Object> items;

    /**
     * 总数
     */
    protected long total;

    /**
     * 每页显示条数，默认 10
     */
    protected long limit;

    /**
     * 当前页
     */
    protected long page;

}
