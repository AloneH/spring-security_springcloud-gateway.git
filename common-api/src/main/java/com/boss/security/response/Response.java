package com.boss.security.response;

import lombok.Data;

/**
 * 统一响应类型
 *
 * @author AloneH
 * @date 2020-07-25
 */
@Data
public class Response {

	/**
	 * 状态码
	 */
	private Integer status;

	/**
	 * 信息
	 */
	private String msg;

	/**
	 * 数据
	 */
	private Object data;

}
