package com.boss.security.controller;


import com.boss.security.dto.PageDTO;
import com.boss.security.dto.RoleDTO;
import com.boss.security.model.ResultType;
import com.boss.security.service.impl.RoleServiceImpl;
import com.boss.security.utils.ResponseUtil;
import com.boss.security.vo.PageVO;
import com.boss.security.vo.RoleVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@RestController
@RequestMapping("role")
public class RoleController {

    @Autowired
    RoleServiceImpl roleService;

    /**
     * 插入数据
     * @param roleVO
     * @return
     */
    @PostMapping("save")
    public String save(@RequestBody RoleVO roleVO) {
        RoleDTO roleDTO = new RoleDTO();
        BeanUtils.copyProperties(roleVO, roleDTO);
        Boolean result = roleService.save(roleDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 更新数据
     * @param roleVO
     * @return
     */
    @PutMapping("update")
    public String update(@RequestBody RoleVO roleVO) {
        RoleDTO roleDTO = new RoleDTO();
        BeanUtils.copyProperties(roleVO, roleDTO);
        Boolean result = roleService.update(roleDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 通过Id读取
     * @param roleVO
     * @return
     */
    @GetMapping("getById")
    public String getById(@RequestParam RoleVO roleVO) {
        RoleDTO roleDTO = new RoleDTO();
        BeanUtils.copyProperties(roleVO, roleDTO);
        RoleDTO result = roleService.getById(roleDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 读取全部
     * @return
     */
    @GetMapping("list")
    public String listAll() {
        List<RoleVO> roleVOList = new ArrayList<>();
        List<RoleDTO> roleDTOList = roleService.listAll();
        for (RoleDTO roleDTO : roleDTOList) {
            RoleVO roleVO = new RoleVO();
            BeanUtils.copyProperties(roleDTO, roleVO);
            roleVOList.add(roleVO);
        }
        return ResponseUtil.getResponse(roleVOList, ResultType.OK);
    }

    /**
     * 通过ID读取
     * @return
     */
    @GetMapping("listByUserId")
    public String listByUserId() {
        List<RoleVO> roleVOList = new ArrayList<>();
        List<RoleDTO> roleDTOList = roleService.listByUserId();
        for (RoleDTO roleDTO : roleDTOList) {
            RoleVO roleVO = new RoleVO();
            BeanUtils.copyProperties(roleDTO, roleVO);
            roleVOList.add(roleVO);
        }
        return ResponseUtil.getResponse(roleVOList, ResultType.OK);
    }

    /**
     * 分页读取
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("listByPage")
    public String listByPage(@RequestParam("page") Long page, @RequestParam("limit") Long limit) {
        PageVO pageVo = new PageVO();
        pageVo.setLimit(limit);
        pageVo.setPage(page);
        PageDTO pageDTO = new PageDTO();
        BeanUtils.copyProperties(pageVo, pageDTO);
        PageDTO result = roleService.listByPage(pageDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 删除角色
     * @param id
     * @return
     */
    @DeleteMapping("remove/{id}")
    public String remove(@PathVariable("id") Integer id) {
        RoleVO roleVO = new RoleVO();
        roleVO.setId(id);
        RoleDTO roleDTO = new RoleDTO();
        BeanUtils.copyProperties(roleVO, roleDTO);
        Boolean result = roleService.remove(roleDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

}

