package com.boss.security.controller;


import com.boss.security.dto.PageDTO;
import com.boss.security.dto.PermissionDTO;
import com.boss.security.model.ResultType;
import com.boss.security.service.impl.PermissionServiceImpl;
import com.boss.security.utils.ResponseUtil;
import com.boss.security.vo.PageVO;
import com.boss.security.vo.PermissionVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@RestController
@RequestMapping("permission")
public class PermissionController {

    @Autowired
    PermissionServiceImpl permissionService;

    /**
     * 插入数据
     * @param permissionVO
     * @return
     */
    @PostMapping("save")
    public String save(@RequestBody PermissionVO permissionVO) {
        PermissionDTO permissionDTO = new PermissionDTO();
        BeanUtils.copyProperties(permissionVO, permissionDTO);
        Boolean result = permissionService.save(permissionDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 更新数据
     * @param permissionVO
     * @return
     */
    @PutMapping("update")
    public String update(@RequestBody PermissionVO permissionVO) {
        PermissionDTO permissionDTO = new PermissionDTO();
        BeanUtils.copyProperties(permissionVO, permissionDTO);
        Boolean result = permissionService.update(permissionDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 通过ID查询
     * @param permissionVO
     * @return
     */
    @GetMapping("getById")
    public String getById(@RequestParam PermissionVO permissionVO) {
        PermissionDTO permissionDTO = new PermissionDTO();
        BeanUtils.copyProperties(permissionVO, permissionDTO);
        PermissionDTO result = permissionService.getById(permissionDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 查询整体列表
     * @return
     */
    @GetMapping("list")
    public String listAll() {
        List<PermissionVO> permissionVOList = new ArrayList<>();
        List<PermissionDTO> permissionDTOList = permissionService.listAll();
        for (PermissionDTO permissionDTO : permissionDTOList) {
            PermissionVO permissionVO = new PermissionVO();
            BeanUtils.copyProperties(permissionDTO, permissionVO);
            permissionVOList.add(permissionVO);
        }
        return ResponseUtil.getResponse(permissionVOList, ResultType.OK);
    }

    /**
     * 通过分页查询
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("listByPage")
    public String listByPage(@RequestParam("page") Long page, @RequestParam("limit") Long limit) {
        PageVO pageVo = new PageVO();
        pageVo.setLimit(limit);
        pageVo.setPage(page);
        PageDTO pageDTO = new PageDTO();
        BeanUtils.copyProperties(pageVo, pageDTO);
        PageDTO result = permissionService.listByPage(pageDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 查询数据
     * @param id
     * @return
     */
    @DeleteMapping("remove/{id}")
    public String remove(@PathVariable("id") Integer id) {
        PermissionVO permissionVO = new PermissionVO();
        permissionVO.setId(id);
        PermissionDTO permissionDTO = new PermissionDTO();
        BeanUtils.copyProperties(permissionVO, permissionDTO);
        Boolean result = permissionService.remove(permissionDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

}

