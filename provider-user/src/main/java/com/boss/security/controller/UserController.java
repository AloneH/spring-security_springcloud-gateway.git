package com.boss.security.controller;


import com.boss.security.dto.PageDTO;
import com.boss.security.dto.UserDTO;
import com.boss.security.model.ResultType;
import com.boss.security.service.impl.RoleServiceImpl;
import com.boss.security.service.impl.UserServiceImpl;
import com.boss.security.utils.ResponseUtil;
import com.boss.security.vo.PageVO;
import com.boss.security.vo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@RestController
@RequestMapping("user")
@Slf4j
public class UserController {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    RoleServiceImpl roleService;

    /**
     * 插入用户
     * @param userVO
     * @return
     */
    @PostMapping("save")
    public String save(@RequestBody UserVO userVO) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userVO, userDTO);
        Boolean result = userService.save(userDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 更新用户
     * @param userVO
     * @return
     */
    @PutMapping("update")
    public String update(@RequestBody UserVO userVO) {
        UserDTO userDTO = new UserDTO();
        log.info(userVO.toString());
        BeanUtils.copyProperties(userVO, userDTO);
        Boolean result = userService.update(userDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 通过ID读取
     * @param id
     * @return
     */
    @GetMapping("getById")
    public String getById(@RequestParam("id") Integer id) {
        UserVO userVO = new UserVO();
        userVO.setId(id);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userVO, userDTO);
        UserDTO result = userService.getById(userDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 读取用户列表
     * @return
     */
    @GetMapping("list")
    public String listAll() {
        List<UserVO> userVOList = new ArrayList<>();
        List<UserDTO> userDTOList = userService.listAll();
        for (UserDTO userDTO : userDTOList) {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(userDTO, userVO);
            userVOList.add(userVO);
        }
        return ResponseUtil.getResponse(userVOList, ResultType.OK);
    }

    /**
     * 分页读取
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("listByPage")
    public String listByPage(@RequestParam("page") Long page, @RequestParam("limit") Long limit) {
        PageVO pageVo = new PageVO();
        pageVo.setLimit(limit);
        pageVo.setPage(page);
        PageDTO pageDTO = new PageDTO();
        BeanUtils.copyProperties(pageVo, pageDTO);
        PageDTO result = userService.listByPage(pageDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 删除用户
     * @param id
     * @return
     */
    @DeleteMapping("remove/{id}")
    public String remove(@PathVariable("id") Integer id) {
        UserVO userVO = new UserVO();
        userVO.setId(id);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userVO, userDTO);
        Boolean result = userService.remove(userDTO);
        return ResponseUtil.getResponse(result, ResultType.OK);
    }

    /**
     * 登录接口 预留
     * @param userVO
     * @param httpServletResponse
     * @return
     */
    @PostMapping("login")
    public String login(@RequestBody UserVO userVO, HttpServletResponse httpServletResponse) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userVO, userDTO);
        userDTO = userService.login(userDTO);
        if (null == userDTO) {
            return ResponseUtil.getResponse(null, ResultType.OK);
        } else {
            return ResponseUtil.getResponse(userVO, ResultType.OK);
        }
    }

    /**
     * 用户信息读取接口 预留
     * @param username
     * @return
     */
    @GetMapping("getInfo")
    public String getInfo(@RequestParam("username") String username) {
        UserVO userVO = new UserVO();
        userVO.setUsername(username);
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userVO, userDTO);
        userDTO = userService.getInfo(userDTO);
        if (null == userDTO) {
            return ResponseUtil.getResponse(null, ResultType.OK);
        } else {
            BeanUtils.copyProperties(userDTO, userVO);
            userVO.setToken(userVO.getUsername());
            return ResponseUtil.getResponse(userVO, ResultType.OK);
        }
    }

}

