package com.boss.security.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.security.dao.RoleDao;
import com.boss.security.dto.PageDTO;
import com.boss.security.dto.RoleDTO;
import com.boss.security.po.RolePO;
import com.boss.security.service.RoleService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, RolePO> implements RoleService {

    /**
     * 插入角色
     * @param entity
     * @return
     */
    public Boolean save(RoleDTO entity) {
        RolePO rolePO = new RolePO();
        BeanUtils.copyProperties(entity, rolePO);
        return super.save(rolePO);
    }

    /**
     * 删除角色
     * @param entity
     * @return
     */
    public Boolean remove(RoleDTO entity) {
        RolePO rolePO = new RolePO();
        BeanUtils.copyProperties(entity, rolePO);
        return super.removeById(rolePO.getId());
    }

    /**
     * 更新角色
     * @param entity
     * @return
     */
    public Boolean update(RoleDTO entity) {
        RolePO rolePO = super.getById(entity.getId());
        BeanUtils.copyProperties(entity, rolePO);
        return super.updateById(rolePO);
    }

    public RoleDTO getById(RoleDTO entity) {
        RolePO rolePO = super.getById(entity.getId());
        BeanUtils.copyProperties(rolePO, entity);
        return entity;
    }

    /**
     * 读取角色列表
     * @return
     */
    public List<RoleDTO> listAll() {
        List<RoleDTO> roleDTOList = new ArrayList<>();
        List<RolePO> rolePOList = super.list();
        for (RolePO rolePO : rolePOList) {
            RoleDTO roleDTO = new RoleDTO();
            BeanUtils.copyProperties(rolePO, roleDTO);
            roleDTOList.add(roleDTO);
        }
        return roleDTOList;
    }

    /**
     * 通过UID读取角色
     * @return
     */
    public List<RoleDTO> listByUserId() {
        List<RoleDTO> roleDTOList = new ArrayList<>();
        List<RolePO> rolePOList = super.list();
        for (RolePO rolePO : rolePOList) {
            RoleDTO roleDTO = new RoleDTO();
            BeanUtils.copyProperties(rolePO, roleDTO);
            roleDTOList.add(roleDTO);
        }
        return roleDTOList;
    }

    /**
     * 分页读取
     * @param pageDTO
     * @return
     */
    public PageDTO listByPage(PageDTO pageDTO) {
        List<Object> roleDTOList = new ArrayList<>();
        Page<RolePO> roleDTOPage = new Page<>();
        // 设置分页信息
        roleDTOPage.setCurrent(pageDTO.getPage());
        roleDTOPage.setSize(pageDTO.getLimit());
        // 读取分页数据
        Page<RolePO> rolePOPage = super.page(roleDTOPage);
        List<RolePO> records = rolePOPage.getRecords();
        for (RolePO record : records) {
            RoleDTO roleDTO = new RoleDTO();
            BeanUtils.copyProperties(record, roleDTO);
            roleDTOList.add(roleDTO);
        }
        pageDTO.setTotal(rolePOPage.getTotal());
        pageDTO.setItems(roleDTOList);
        return pageDTO;
    }

}
