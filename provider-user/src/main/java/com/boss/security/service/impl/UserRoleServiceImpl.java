package com.boss.security.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.security.dao.UserRoleDao;
import com.boss.security.po.UserRolePO;
import com.boss.security.service.UserRoleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author AloneH
 * @since 2020-07-27
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDao, UserRolePO> implements UserRoleService {

}
