package com.boss.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boss.security.po.UserRolePO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author AloneH
 * @since 2020-07-27
 */
public interface UserRoleService extends IService<UserRolePO> {

}
