package com.boss.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boss.security.po.PermissionPO;
import com.boss.security.po.RolePO;

/**
 * @InterfaceName PermissionService
 * @Author AloneH
 * @Date 2020/7/29 20:53
 * @Description TODO
 **/
public interface PermissionService extends IService<PermissionPO> {

}