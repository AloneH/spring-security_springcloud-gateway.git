package com.boss.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.boss.security.dto.PageDTO;
import com.boss.security.dto.UserDTO;
import com.boss.security.dao.UserDao;
import com.boss.security.po.RolePO;
import com.boss.security.po.UserPO;
import com.boss.security.po.UserRolePO;
import com.boss.security.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, UserPO> implements UserService {

    @Autowired
    HttpServletRequest httpServletRequest;

    @Autowired
    UserRoleServiceImpl userRoleService;

    /**
     * 更新用户
     * @param entity
     * @return
     */
    public Boolean save(UserDTO entity) {
        UserPO userPO = new UserPO();
        BeanUtils.copyProperties(entity, userPO);
        super.save(userPO);
        return super.save(userPO);
    }

    /**
     * 删除用户
     * @param entity
     * @return
     */
    public Boolean remove(UserDTO entity) {
        UserPO userPO = new UserPO();
        BeanUtils.copyProperties(entity, userPO);
        return super.removeById(userPO.getId());
    }

    /**
     * 更新用户
     * @param entity
     * @return
     */
    public Boolean update(UserDTO entity) {
        UserPO userPO = super.getById(entity.getId());
        BeanUtils.copyProperties(entity, userPO);
        // 更新用户信
        boolean userResult = super.updateById(userPO);
        //
        List<UserRolePO> userRolePOList = new ArrayList<>();
        List<RolePO> roles = userPO.getRoles();
        if (roles != null) {
            for (RolePO rolePO : roles) {
                UserRolePO userRolePO = new UserRolePO();
                userRolePO.setUid(userPO.getId());
                userRolePO.setRid(rolePO.getId());
                userRolePOList.add(userRolePO);
            }
        }
        // 构建删除条件
        QueryWrapper<UserRolePO> removeQueryWrapper = new QueryWrapper<>();
        removeQueryWrapper.eq("uid", userPO.getId());
        userRoleService.remove(removeQueryWrapper);
        boolean roleResult = userRoleService.saveBatch(userRolePOList);
        return roleResult && userResult;
    }

    /**
     * 通过ID读取
     * @param entity
     * @return
     */
    public UserDTO getById(UserDTO entity) {
        UserPO userPO = super.getById(entity.getId());
        BeanUtils.copyProperties(userPO, entity);
        return entity;
    }

    /**
     * 读取全部
     * @return
     */
    public List<UserDTO> listAll() {
        List<UserDTO> userDTOList = new ArrayList<>();
        List<UserPO> userPOList = super.list();
        for (UserPO userPO : userPOList) {
            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(userPO, userDTO);
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    /**
     * 分页读取
     * @param pageDTO
     * @return
     */
    public PageDTO listByPage(PageDTO pageDTO) {
        List<Object> userDTOList = new ArrayList<>();
        Page<UserPO> userDTOPage = new Page<>();
        // 设置分页信息
        userDTOPage.setCurrent(pageDTO.getPage());
        userDTOPage.setSize(pageDTO.getLimit());
        // 读取分页数据
        Page<UserPO> userPOPage = super.page(userDTOPage);

        List<UserPO> records = userPOPage.getRecords();
        for (UserPO record : records) {
            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(record, userDTO);
            userDTOList.add(userDTO);
        }
        pageDTO.setTotal(userPOPage.getTotal());
        pageDTO.setItems(userDTOList);
        return pageDTO;
    }

    /**
     * 登录
     * @param userDTO
     * @return
     */
    public UserDTO login(UserDTO userDTO) {
        QueryWrapper<UserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", userDTO.getUsername());
        queryWrapper.eq("password", userDTO.getPassword());
        UserPO userPO = super.getOne(queryWrapper);
        if (userPO == null) {
            return null;
        }
        userPO.setToken(UUID.randomUUID().toString());
        BeanUtils.copyProperties(userPO, userDTO);
        return userDTO;
    }

    /**
     * 获取用户信息
     * @param userDTO
     * @return
     */
    public UserDTO getInfo(UserDTO userDTO) {
        UserPO userPO = super.getBaseMapper().selectByUsername(userDTO.getUsername());
        if (userPO == null) {
            return null;
        }
        BeanUtils.copyProperties(userPO, userDTO);
        return userDTO;
    }

}
