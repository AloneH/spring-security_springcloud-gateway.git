package com.boss.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boss.security.po.UserPO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
public interface UserService extends IService<UserPO> {

}