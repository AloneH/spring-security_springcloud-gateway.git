package com.boss.security.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.security.dao.PermissionDao;
import com.boss.security.dto.PageDTO;
import com.boss.security.dto.PermissionDTO;
import com.boss.security.po.PermissionPO;
import com.boss.security.service.PermissionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author AloneH
 * @since 2020-07-25
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionDao, PermissionPO> implements PermissionService {

    /**
     * 保存接口
     * @param entity
     * @return
     */
    public Boolean save(PermissionDTO entity) {
        PermissionPO permissionPO = new PermissionPO();
        BeanUtils.copyProperties(entity, permissionPO);
        return super.save(permissionPO);
    }

    /**
     * 删除数据
     * @param entity
     * @return
     */
    public Boolean remove(PermissionDTO entity) {
        PermissionPO permissionPO = new PermissionPO();
        BeanUtils.copyProperties(entity, permissionPO);
        return super.removeById(permissionPO.getId());
    }

    /**
     * 更新数据
     * @param entity
     * @return
     */
    public Boolean update(PermissionDTO entity) {
        PermissionPO permissionPO = super.getById(entity.getId());
        BeanUtils.copyProperties(entity, permissionPO);
        return super.updateById(permissionPO);
    }

    /**
     * 通过ID读取
     * @param entity
     * @return
     */
    public PermissionDTO getById(PermissionDTO entity) {
        PermissionPO permissionPO = super.getById(entity.getId());
        BeanUtils.copyProperties(permissionPO, entity);
        return entity;
    }

    /**
     * 读取全部
     * @return
     */
    public List<PermissionDTO> listAll() {
        List<PermissionDTO> permissionDTOList = new ArrayList<>();
        List<PermissionPO> permissionPOList = super.list();
        for (PermissionPO permissionPO : permissionPOList) {
            PermissionDTO permissionDTO = new PermissionDTO();
            BeanUtils.copyProperties(permissionPO, permissionDTO);
            permissionDTOList.add(permissionDTO);
        }
        return permissionDTOList;
    }

    /**
     * 分页读取
     * @param pageDTO
     * @return
     */
    public PageDTO listByPage(PageDTO pageDTO) {
        List<Object> roleDTOList = new ArrayList<>();
        Page<PermissionPO> roleDTOPage = new Page<>();
        // 设置分页信息
        roleDTOPage.setCurrent(pageDTO.getPage());
        roleDTOPage.setSize(pageDTO.getLimit());
        // 读取分页数据
        Page<PermissionPO> rolePOPage = super.page(roleDTOPage);
        List<PermissionPO> records = rolePOPage.getRecords();
        for (PermissionPO record : records) {
            PermissionDTO roleDTO = new PermissionDTO();
            BeanUtils.copyProperties(record, roleDTO);
            roleDTOList.add(roleDTO);
        }
        pageDTO.setTotal(rolePOPage.getTotal());
        pageDTO.setItems(roleDTOList);
        return pageDTO;
    }
}
