package com.boss.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boss.security.po.RolePO;
import com.boss.security.po.UserPO;

/**
 * @InterfaceName RoleService
 * @Author AloneH
 * @Date 2020/7/29 20:52
 * @Description TODO
 **/

public interface RoleService extends IService<RolePO> {

}
