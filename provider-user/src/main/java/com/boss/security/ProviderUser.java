package com.boss.security;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName ProviderUser
 * @Author AloneH
 * @Date 2020/7/25 10:50
 * @Description TODO
 **/

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.boss.security.dao")
public class ProviderUser {
    public static void main(String[] args) {
        SpringApplication.run(ProviderUser.class, args);
    }
}
