package com.boss.security.auth;

import com.boss.security.entity.AuthRoleGrantedAuthority;
import com.boss.security.entity.AuthUserDetails;
import com.boss.security.model.ResultType;
import com.boss.security.po.RolePO;
import com.boss.security.po.UserPO;
import com.boss.security.utils.ResponseUtil;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.ServerAuthenticationEntryPoint;
import org.springframework.security.web.server.authentication.HttpBasicServerAuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * @ClassName CustomServerAuthenticationEntryPoint
 * @Author AloneH
 * @Date 2020/7/27 17:33
 * @Description
 *              自定义token验证失败处理逻辑
 *                  1、token验证失败的处理逻辑
 *                  2、设置响应头
 *                  3、写入响应内容
 **/

@Component
public class CustomServerAuthenticationEntryPoint implements ServerAuthenticationEntryPoint {

    private static final String WWW_AUTHENTICATE = "Authenticate";
    private static final String DEFAULT_REALM = "{Token}";
    private static String WWW_AUTHENTICATE_FORMAT = "Basic %s";

    private String headerValue = createHeaderValue(DEFAULT_REALM);

    @Override
    public Mono<Void> commence(ServerWebExchange exchange, AuthenticationException e) {
        ServerHttpResponse response = exchange.getResponse();
        // 设置响应头
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        response.getHeaders().set(WWW_AUTHENTICATE, this.headerValue);
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON_UTF8);
        // 三设置 验证异常返回信息
        String responseBodyString = ResponseUtil.getResponse(e.getMessage(), ResultType.UNAUTHORIZED);
        byte[] responseBodyBytes = responseBodyString.getBytes();
        // 写入信息
        return response.writeWith(Mono.just(response.bufferFactory().wrap(responseBodyBytes)));
    }

    /**
     * 创建错误响应头中的Authenticate
     * @param realm
     * @return
     */
    private static String createHeaderValue(String realm) {
        Assert.notNull(realm, "realm cannot be null");
        return String.format(WWW_AUTHENTICATE_FORMAT, realm);
    }

}
