package com.boss.security.auth;

import com.boss.security.service.impl.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Base64;

/**
 * @ClassName CustmoServerSecurityContextRepository
 * @Author AloneH
 * @Date 2020/7/28 16:53
 * @Description
 *              自定义认证上下文存储器
 *                  1、 检查Authorization请求头是否存在，不存在返回空
 *                  2、 检查token格式是否正确，不正确返回空
 *                  3、 返回一个UsernamePasswordAuthenticationToken，第一个参数为用户名，第二个参数为密码
 **/

@Component
@Slf4j
public class CustmoServerSecurityContextRepository implements ServerSecurityContextRepository {

    @Autowired
    CustmoReactiveAuthenticationManager authenticationManager;

    @Autowired
    UserServiceImpl userService;

    @Override
    public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
        return Mono.empty();
    }

    @Override
    public Mono<SecurityContext> load(ServerWebExchange exchange) {
        ServerHttpRequest request = exchange.getRequest();
        String authHeader = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        if (authHeader != null && authHeader.startsWith("Basic ")) {

            // 自定义token处理逻辑开始
            String authToken = authHeader.replace("Basic ", "");
            Authentication auth = new UsernamePasswordAuthenticationToken(null, null);
            String str = new String(Base64.getDecoder().decode(authToken));
            // 得到username和token
            String[] split = str.split(":", 2);
            if (split.length != 2 || split.length < 2) {
                // token格式错误
                return this.authenticationManager.authenticate(auth).map(SecurityContextImpl::new);
            }
            String username = split[0].substring(36);
            String password = split[1].substring(36);
            // 自定义token处理逻辑结束
            // 设置上下文信息
            auth = new UsernamePasswordAuthenticationToken(username, password);
            return this.authenticationManager.authenticate(auth).map(SecurityContextImpl::new);
        } else {
            return Mono.empty();
        }
    }
}

