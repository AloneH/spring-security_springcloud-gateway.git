package com.boss.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.security.dao.UserDao;
import com.boss.security.entity.AuthRoleGrantedAuthority;
import com.boss.security.entity.AuthUserDetails;
import com.boss.security.po.PermissionPO;
import com.boss.security.po.RolePO;
import com.boss.security.po.UserPO;
import com.boss.security.service.UserService;
import com.boss.security.vo.PermissionVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName UserServiceImpl
 * @Author AloneH
 * @Date 2020/7/28 8:25
 * @Description TODO
 **/

@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserDao, UserPO> implements UserService {

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        UserPO userPO = this.selectByUsername(username);
        if (userPO == null) {
                return Mono.empty();
        }
        AuthUserDetails authUserDetails = new AuthUserDetails()
                .setId(userPO.getId())
                .setUsername(userPO.getUsername())
                .setPassword(userPO.getPassword());
        List<RolePO> roles = userPO.getRoles();
        List<GrantedAuthority> authRoleGrantedAuthorityList = new ArrayList<>();
        if (roles != null) {
            // 添加角色列表
            for (RolePO role : roles) {
                AuthRoleGrantedAuthority authRoleGrantedAuthority = new AuthRoleGrantedAuthority();
                List<PermissionVO> permissionVOList = new ArrayList<>();
                List<PermissionPO> permissions = role.getPermissions();
                if (permissions != null) {
                    // 添加权限列表
                    for (PermissionPO permission : permissions) {
                        PermissionVO permissionVO = new PermissionVO();
                        permissionVO.setId(permission.getId());
                        permissionVO.setName(permission.getName());
                        permissionVO.setUrl(permission.getUrl());
                        permissionVOList.add(permissionVO);
                    }
                }
                authRoleGrantedAuthority.setId(role.getId());
                authRoleGrantedAuthority.setName(role.getName());
                authRoleGrantedAuthority.setRole(role.getRole());
                authRoleGrantedAuthority.setPermissions(permissionVOList);
                authRoleGrantedAuthorityList.add(authRoleGrantedAuthority);
            }
        }
        authUserDetails.setRoles(authRoleGrantedAuthorityList);
        log.info(authUserDetails.toString());
        return Mono.just(authUserDetails);
    }

    public UserPO selectByUsername(String username) {
        return this.getBaseMapper().selectByUsername(username);
    }

    // @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserPO userPO = this.selectByUsername(username);
        if (userPO == null) {
            return null;
        }
        AuthUserDetails authUserDetails = new AuthUserDetails()
                .setId(userPO.getId())
                .setUsername(userPO.getUsername())
                .setPassword(userPO.getPassword());
        List<RolePO> roles = userPO.getRoles();
        List<GrantedAuthority> authRoleGrantedAuthorityList = new ArrayList<>();
        if (roles != null) {
            for (RolePO role : roles) {
                AuthRoleGrantedAuthority authRoleGrantedAuthority = new AuthRoleGrantedAuthority();
                authRoleGrantedAuthority.setId(role.getId());
                authRoleGrantedAuthority.setName(role.getName());
                authRoleGrantedAuthority.setRole(role.getRole());
                authRoleGrantedAuthorityList.add(authRoleGrantedAuthority);
            }
        }
        authUserDetails.setRoles(authRoleGrantedAuthorityList);
        return authUserDetails;
    }

    public UserPO selectByUsernameAndToken(String username, String token) {
        QueryWrapper<UserPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        queryWrapper.eq("token", token);
        return super.getOne(queryWrapper);
    }
}
