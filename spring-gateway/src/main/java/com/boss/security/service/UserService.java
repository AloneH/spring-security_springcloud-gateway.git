package com.boss.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boss.security.po.UserPO;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;

/**
 * @InterfaceName UserService
 * @Author AloneH
 * @Date 2020/7/28 8:24
 * @Description TODO
 **/
public interface UserService extends ReactiveUserDetailsService, IService<UserPO> {

}
