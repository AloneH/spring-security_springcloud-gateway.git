package com.boss.security.entity;

import com.boss.security.vo.PermissionVO;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

/**
 * @ClassName AuthRoleGrantedAuthority
 * @Author AloneH
 * @Date 2020/7/28 8:30
 * @Description
 *              Security授权角色信息的继承类
 *                  1、 增加自定义的部分字段
 *                  2、 getAuthority方法返回角色
 *              将角色具有的权限定义再列表中
 **/

public class AuthRoleGrantedAuthority implements GrantedAuthority {

    private Integer id;

    private String role;

    private String name;

    private List<PermissionVO> permissions;

    @Override
    public String getAuthority() {
        return this.role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PermissionVO> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionVO> permissions) {
        this.permissions = permissions;
    }
}
