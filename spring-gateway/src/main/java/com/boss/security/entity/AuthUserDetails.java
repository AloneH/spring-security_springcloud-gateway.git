package com.boss.security.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @ClassName AuthUserDetails
 * @Author AloneH
 * @Date 2020/7/27 20:30
 * @Description
 * UserDetail
 *            实体的继承类
 *              1、添加自定义部分内容
 *              2、确保用户名和密钥字段为username、password并且具有setter和getter
 **/

@Data
@Accessors(chain = true)
public class AuthUserDetails implements UserDetails {

    private Integer id;

    private String username;

    private String password;

    private List<GrantedAuthority> roles;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
