package com.boss.security;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;

/**
 * @ClassName SpringGateway
 * @Author AloneH
 * @Date 2020/7/27 15:16
 * @Description TODO
 **/

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = {"com.boss.security.dao"})
@RefreshScope
public class SpringGateway {

    public static void main(String[] args) {
        SpringApplication.run(SpringGateway.class, args);
    }

}
