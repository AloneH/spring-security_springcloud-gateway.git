package com.boss.security.handler;

import com.boss.security.entity.AuthRoleGrantedAuthority;
import com.boss.security.entity.AuthUserDetails;
import com.boss.security.model.ResultType;
import com.boss.security.po.UserPO;
import com.boss.security.service.impl.UserServiceImpl;
import com.boss.security.utils.ResponseUtil;
import com.boss.security.vo.PermissionVO;
import com.boss.security.vo.RoleVO;
import com.boss.security.vo.UserVO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.server.WebFilterExchange;
import org.springframework.security.web.server.authentication.WebFilterChainServerAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.*;

/**
 * @ClassName CustomAuthenticationSuccessHandler
 * @Author AloneH
 * @Date 2020/7/27 17:13
 * @Description
 *              自定义认证成功处理逻辑
 *                  1、 设置响应头
 *                  2、 根据用户名和密码生成token
 *                  3、 设置返回信息
 *                  4、 写入响应信息
 **/

@Component
public class CustomAuthenticationSuccessHandler extends WebFilterChainServerAuthenticationSuccessHandler {

    @Autowired
    UserServiceImpl userService;

    @Override
    public Mono<Void> onAuthenticationSuccess(WebFilterExchange webFilterExchange, Authentication authentication) {
        ServerWebExchange exchange = webFilterExchange.getExchange();
        ServerHttpResponse response = exchange.getResponse();
        //设置headers
        HttpHeaders httpHeaders = response.getHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=UTF-8");
        httpHeaders.add("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
        //设置body
        byte[] dataBytes = {};
        ObjectMapper mapper = new ObjectMapper();
        try {
            // 获取验证对象
            AuthUserDetails userDetails = (AuthUserDetails) authentication.getPrincipal();
            // 获取用户输入的密码
            String credentials = (String) authentication.getCredentials();
            // 获取角色信息
            Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) authentication.getAuthorities();
            byte[] authorization = (UUID.randomUUID().toString() + userDetails.getUsername() + ":" + UUID.randomUUID().toString() + credentials).getBytes();
            String token = Base64.getEncoder().encodeToString(authorization);
            httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic " + token);
            // 更新数据库Token
            UserVO userVO = new UserVO();
            userVO.setId(userDetails.getId());
            userVO.setUsername(userDetails.getUsername());
            List<GrantedAuthority> roles = userDetails.getRoles();
            List<RoleVO> roleVOList = new ArrayList<>();
            // 循环添加用户角色和权限
            for (GrantedAuthority authority : authorities) {
                AuthRoleGrantedAuthority authRoleGrantedAuthority = (AuthRoleGrantedAuthority) authority;
                RoleVO roleVO = new RoleVO();
                roleVO.setId(authRoleGrantedAuthority.getId());
                roleVO.setName(authRoleGrantedAuthority.getName());
                roleVO.setRole(authRoleGrantedAuthority.getRole());
                roleVO.setPermissions(authRoleGrantedAuthority.getPermissions());
                roleVOList.add(roleVO);
            }
            userVO.setRoles(roleVOList);
            userVO.setToken(token);
            dataBytes = ResponseUtil.getResponse(userVO, ResultType.OK).getBytes();
        } catch (Exception ex) {
            ex.printStackTrace();
            dataBytes = ResponseUtil.getResponse(ex.getMessage(), ResultType.FAILED_DEPENDENCY).getBytes();
        }

        DataBuffer bodyDataBuffer = response.bufferFactory().wrap(dataBytes);
        return response.writeWith(Mono.just(bodyDataBuffer));
    }


}
