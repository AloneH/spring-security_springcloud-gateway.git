package com.boss.security.handler;

import com.boss.security.model.ResultType;
import com.boss.security.utils.ResponseUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.server.authorization.ServerAccessDeniedHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName CustomAuthenticationAccessDeniedHandler
 * @Author AloneH
 * @Date 2020/7/29 15:51
 * @Description
 *              自定义无权限访问的异常处理
 *                  1、设置响应头
 *                  2、返回无权限信息
 **/

@Component
public class CustomAuthenticationAccessDeniedHandler implements ServerAccessDeniedHandler {
    @Override
    public Mono<Void> handle(ServerWebExchange exchange, AccessDeniedException denied) {
        ServerHttpResponse response = exchange.getResponse();
        // 设置响应头
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON_UTF8);
        // 三设置 验证异常返回信息
        String responseBodyString = ResponseUtil.getResponse(denied.getMessage(), ResultType.FORBIDDEN);
        byte[] responseBodyBytes = responseBodyString.getBytes();
        // 写入信息
        return response.writeWith(Mono.just(response.bufferFactory().wrap(responseBodyBytes)));
    }
}
