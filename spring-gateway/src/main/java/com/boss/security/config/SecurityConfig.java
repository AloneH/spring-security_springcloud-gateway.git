package com.boss.security.config;

import com.boss.security.auth.CustmoReactiveAuthenticationManager;
import com.boss.security.auth.CustmoServerSecurityContextRepository;
import com.boss.security.auth.CustomReactiveAuthorizationManager;
import com.boss.security.handler.CustomAuthenticationAccessDeniedHandler;
import com.boss.security.handler.CustomAuthenticationFailureHandler;
import com.boss.security.handler.CustomAuthenticationSuccessHandler;
import com.boss.security.auth.CustomServerAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.server.SecurityWebFilterChain;

/**
 * @ClassName SecurityConfig
 * @Author AloneH
 * @Date 2020/7/27 15:56
 * @Description
<<<<<<< HEAD
 *              认证链配置器
 *                  1、自定义认证上下文存储库----获取和更改上下文信息
 *                  2、自定义认证处理器
 *                  3、自定义失败处理器
 *                  4、自定义成功处理器
 *                  5、自定义token认证失败处理器
 *              配置连中的先后顺序尤其重要，认证链根据加入的先后顺序按顺序执行，而不是一次性全部载入系统。
=======
 * Secutrity 配置类
>>>>>>> 1ebd98c79e356e1197d93ef552c1e845a47e16a0
 **/

@EnableWebFluxSecurity
public class SecurityConfig {

    @Autowired
    CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Autowired
    CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @Autowired
    CustomServerAuthenticationEntryPoint customServerAuthenticationEntryPoint;

    @Autowired
    CustmoServerSecurityContextRepository custmoServerSecurityContextRepository;

    @Autowired
    CustmoReactiveAuthenticationManager custmoReactiveAuthenticationManager;

    @Autowired
    CustomAuthenticationAccessDeniedHandler customAuthenticationAccessDeniedHandler;

    @Autowired
    CustomReactiveAuthorizationManager customReactiveAuthorizationManager;

    private static final String[] excludedAuthPages = {
            "/user/login",
            "/user/logout"
    };

    /**
     * 配置认证链
     * @param http
     * @return
     */
    @Bean
    SecurityWebFilterChain webFilterChain(ServerHttpSecurity http) {

        http.authorizeExchange()
                .pathMatchers(excludedAuthPages).permitAll()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .pathMatchers("/**").access(customReactiveAuthorizationManager)
                .anyExchange().authenticated()
                .and()
                .securityContextRepository(custmoServerSecurityContextRepository)
                .authenticationManager(custmoReactiveAuthenticationManager)
                .formLogin().loginPage("/user/login")
                .authenticationSuccessHandler(customAuthenticationSuccessHandler)
                .authenticationFailureHandler(customAuthenticationFailureHandler)
                .and()
                .httpBasic().disable()
                .csrf().disable()
                .logout().disable();

        http.exceptionHandling()
                .accessDeniedHandler(customAuthenticationAccessDeniedHandler)
                .authenticationEntryPoint(customServerAuthenticationEntryPoint);


        return http.build();
    }

    /**
     * 配置密码加密方式 返回Bean
     * @return
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
